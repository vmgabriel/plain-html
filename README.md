![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

## Creator
(**vmgabriel** or **v_mgabriel**) Gabriel Vargas Monroy

## Page
[https:/gitlab.io/vmgabriel/curriculum-vitae](https:/gitlab.io/vmgabriel/curriculum-vitae "Pagina")

## Licence

Using GitLab Pages.
[ci]: https://about.gitlab.com/gitlab-ci/
[index.html]: https://gitlab.com/pages/plain-html/blob/master/public/index.html
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
